﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CodingKatas;

namespace CodingKatas.UnitTests
{
    [TestClass]
    public class PrimeNumberGeneratorTests
    {
        [TestMethod]
        public void PrimeNumberGeneratorWorks()
        {
            var png = new PrimeNumberGenerator();
            Assert.AreEqual(2, png.GenerateNextPrimeNumber(1));
            Assert.AreEqual(3, png.GenerateNextPrimeNumber(2));
            Assert.AreEqual(5081, png.GenerateNextPrimeNumber(5078));
        }

        [TestMethod]
        public void PrimeNumberGeneratorThrowsArgumentExceptionOnIncorrectValues()
        {
            Exception thrownException = null;
            try
            {
                var png = new PrimeNumberGenerator();
                png.GenerateNextPrimeNumber(-1);
            }
            catch (Exception ex)
            {
                thrownException = ex;
            }
            Assert.IsNotNull(thrownException);
            Assert.IsInstanceOfType(thrownException, typeof(ArgumentOutOfRangeException));
        }
    }
}
