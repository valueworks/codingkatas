﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CodingKatas;

namespace CodingKatas.UnitTests
{
    [TestClass]
    public class FizzBuzzerTests
    {
        [TestMethod]
        public void FizzBuzzerWorks()
        {
            var fb = new FizzBuzzer();

            Assert.AreEqual("11,fizz,13,14,fizzbuzz",fb.FizzBuzz(11,4));
            Assert.AreEqual("4001,fizz,4003,4004,fizzbuzz,4006,4007", fb.FizzBuzz(4001, 6));
            Assert.AreEqual("buzz,49,fizz,47,46,fizzbuzz", fb.FizzBuzz(50, -5));
            Assert.AreEqual("buzz", fb.FizzBuzz(50, 0));
        }

        [TestMethod]
        public void FizzBuzzerThrowExceptionOnZeroOrBelow()
        {
            var fb = new FizzBuzzer();
            Exception thrownException = null;
            try
            {
                var sr = new StringReverser();
                fb.FizzBuzz(-2,1);
            }
            catch (Exception ex)
            {
                thrownException = ex;
            }
            Assert.IsNotNull(thrownException);
            Assert.IsInstanceOfType(thrownException, typeof(ArgumentOutOfRangeException));

            thrownException = null;
            try
            {
                var sr = new StringReverser();
                fb.FizzBuzz(0, 8);
            }
            catch (Exception ex)
            {
                thrownException = ex;
            }
            Assert.IsNotNull(thrownException);
            Assert.IsInstanceOfType(thrownException, typeof(ArgumentOutOfRangeException));
        }
    }
}
