﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CodingKatas;

namespace CodingKatas.UnitTests
{
    [TestClass]
    public class StringReversalTests
    {
        [TestMethod]
        public void StringReversalWorks()
        {
            var sr = new StringReverser();
            Assert.AreEqual("cba", sr.ReverseString("abc"));
            Assert.AreEqual("ILoveValueworks", sr.ReverseString("skroweulaVevoLI"));
        }

        [TestMethod]
        public void StringReversalThrowsArgumentExceptionOnEmptyString()
        {
            Exception thrownException = null;
            try
            {
                var sr = new StringReverser();
                sr.ReverseString(String.Empty);
            }
            catch (Exception ex)
            {
                thrownException = ex;
            }
            Assert.IsNotNull(thrownException);
            Assert.IsInstanceOfType(thrownException, typeof(ArgumentOutOfRangeException));
        }

        [TestMethod]
        public void StringReversalThrowsArgumentExceptionOnNullString()
        {
            Exception thrownException = null;
            try
            {
                var sr = new StringReverser();
                sr.ReverseString(null);
            }
            catch (Exception ex)
            {
                thrownException = ex;
            }
            Assert.IsNotNull(thrownException);
            Assert.IsInstanceOfType(thrownException, typeof(ArgumentNullException));
        }

    }
}
