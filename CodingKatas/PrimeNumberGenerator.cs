﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingKatas
{
    public class PrimeNumberGenerator
    {
        public int GenerateNextPrimeNumber(int startNumber)
        {
            /*
             * Provide the implementation to generate the next prime number (a natural number greater than 1 that has no positive divisors other than 1 and itself) 
             * after the given positive number.
             * 
             * Given the prime 3 your output would be 5 etc.
             * 
             * How would you expect to handle negative (or other out-of-range) values?
             */
            throw new NotImplementedException();
        }
    }
}
