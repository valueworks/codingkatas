﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingKatas
{
    public class FizzBuzzer
    {
        public string FizzBuzz(int startNumber, int numbersToCount)
        {
            /*
             * Provide the implementation for a simple FizzBuzz. Loop through the specified range and where the number is evenly divisible 
             * by 3 (and only 3) output Fizz, where it is evenly divisible by 5 (and only 5) output Buzz and where it is evenly divisible 
             * by 3 AND 5 output FizzBuzz; All other cases output the number.
             * 
             * Given the range 0 to 5 the output should look something like: "FizzBuzz,1,2,Fizz,4,Buzz" etc.
             * 
             * How would you expect to handle a length that is less than the start value?
             * How would you expect to handle single ranges i.e. 10 to 10 etc.?
             */
            throw new NotImplementedException();
        }
    }
}
